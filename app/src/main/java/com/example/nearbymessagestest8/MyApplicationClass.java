package com.example.nearbymessagestest8;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

/**
 * Created By mayurlathkar on 15,August,2019
 */
public class MyApplicationClass extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
    }
}
